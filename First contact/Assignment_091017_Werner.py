from sympy import *
init_printing(use_unicode=True)

# Problem 1.4
# Init symbols
v, x = symbols('v x')

# (i)
phi1 = (1/sqrt(2*pi))*Integral(exp(-(x**2)/2), (x, -oo, v))
phi1_plot = (1/sqrt(2*pi))*Integral(exp(-(x**2)/2), (x, -100, v)) # for plotting, as -oo does not work
p1 = plot(phi1_plot, show=False)
p1.save("1.4.1.png")

# This function is limited to [0, 1] and has a steep angle, similar to the standard Sigmoid function.

# (ii)
phi2 = (2/pi) * atan(v)
p2 = plot(phi2, show=False)
p2.save("1.4.2.png")
print("Phi2 limit to -oo", limit(phi2, v, -oo))
print("Phi2 limit to +oo", limit(phi2, v, +oo))

# This function is limited to [-1, 1] and doesnt reach its limits as fast as the previous function. Fairly similar to the Hyperbolic tangent function.

# Problem 1.7
# Init symbols
a, b, v = symbols('a b v')

# (a)
p = Piecewise((b, v > a), (v, v > -a), (-b, v < -a))

# (b)
p3 = plot(p.subs([(b,1), (a, 0)]))
p3.save("1.7.png")
# It becomes the McCulloch-Pitts Model threshold function
